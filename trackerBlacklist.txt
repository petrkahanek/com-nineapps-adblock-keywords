//**AdBlockList**//
// Don't remove the above line!!!!

/// Common trackers url list

// Analytics
google-analytics.com

ssl.google-analytics.com
hotjar.com
static.hotjar.com
api-hotjar.com
hotjar-analytics.com
mouseflow.com
a.mouseflow.com
freshmarketer.com
luckyorange.com
cdn.luckyorange.com
w1.luckyorange.com
upload.luckyorange.net
cs.luckyorange.net
settings.luckyorange.net
stats.wp.com

// errors
bugsnag.com
browser.sentry-cdn.com
app.getsentry.com


// social
pixel.facebook.com
analytics.facebook.com
ads.facebook.com
an.facebook.com
ads-twitter.com
static.ads-twitter.com
ads-api.twitter.com
advertising.twitter.com
ads.linkedin.com
analytics.pointdrive.linkedin.com
ads.pinterest.com
log.pinterest.com
ads-dev.pinterest.com
analytics.pinterest.com
trk.pinterest.com
widgets.pinterest.com
ads.reddit.com
d.reddit.com
rereddit.com
events.redditmedia.com
ads.youtube.com
youtube.cleverads.vn
analytics.tiktok.com
ads.tiktok.com
analytics-sg.tiktok.com
ads-sg.tiktok.com
analytics.yahoo.com
appmetrica.yandex.com
yandexadexchange.net
analytics.mobile.yandex.net
extmaps-api.yandex.net
adsdk.yandex.ru
